<?php

namespace App\Http\Controllers\Api\Messenger;

use App\Http\Controllers\Controller;
use App\Telegram\Conversations\InitOrderConversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Http\Request;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use Illuminate\Support\Facades\Log;

class TelegramController extends Controller
{
    public function telegram(Request $request) {
        Log::info('Test', $request->all());
        DriverManager::loadDriver(\BotMan\Drivers\Telegram\TelegramDriver::class);

        // Создаем экземпляр
        $botman = BotManFactory::create(config('botman'));

        // Настраиваем бота на прослушивание
        $botman->hears('/test', function (BotMan $bot) {
            $bot->startConversation(new InitOrderConversation());
        });

        // Начинаем прослушивание
        $botman->listen();
    }
}
