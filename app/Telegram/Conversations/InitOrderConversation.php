<?php

namespace App\Telegram\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Support\Facades\Log;

class InitOrderConversation extends \BotMan\BotMan\Messages\Conversations\Conversation
{
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $this->welcomeUser();
    }


    private function welcomeUser()
    {
        //$this->say('Hey '.$this->bot->getUser()->getFirstName().' 👋');
        $this->askIfReady();
    }


    private function askIfReady()
    {
        $question = Question::create('Заказываем от сюда?' . $this->url)
            ->addButtons([
                Button::create('Заказываем')->value('yes'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
                Button::create('Не заказываем')->value('no'),
            ]);

        $this->ask($question, function (Answer $answer) {
            //Log::info("Answer ", []);
            if ($answer->getValue() === 'yes') {
                $this->say('Perfect!');
                return;
            }

            //$this->say('😒');
            $this->say('If you change your opinion, you can start the quiz at any time using the start command or by typing "start".');
        });
    }
}
