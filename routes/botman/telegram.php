<?php

use App\Telegram\Conversations\InitOrderConversation;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;

use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Telegram Listeners
|--------------------------------------------------------------------------
|
| ...
|
*/

Route::any('telegram', function (Request $request) {
    DriverManager::loadDriver(TelegramDriver::class);

    // Создаем экземпляр
    $botman = resolve('botman');

    // Настраиваем бота на прослушивание
    $botman->hears('/init {url}', function (BotMan $bot, string $url) {
        $bot->startConversation(new InitOrderConversation($url));
    });

    // Начинаем прослушивание
    $botman->listen();
});


